import unittest

class TestPersona(unittest.TestCase):

    def test_construir(self):
        e = Persona("Pepe", 20000)
        j = Persona("Ana", 30000, "jefe", 2000)
        self.assertEqual(e.nombre,"Pepe")
        self.assertEqual(j.nombre, "Ana")
        if self.assertEqual(e.puesto, "empleado"):
            self.assertIsNone(e.bonus)
        if self.assertEqual(j.puesto, "jefe"):
            self.assertIsNotNone(j.bonus)
        self.assertEqual(e.sueldo, 20000)
        self.assertEqual(j.sueldo, 30000)
        if self.assertIsNone(e.NIF) or self.assertIsNone(j.NIF):
            nif=input("No ha escrito su NIF por favor páselo a la terminal para evitar irregularidades")
            print(nif)
        self.assertEqual(e.calculo_impuestos(), e.sueldo * 0.30)
        self.assertEqual(j.calculo_impuestos(), (j.sueldo+j.bonus) * 0.30)
        self.assertEqual(e.sueldo_tras_impuestos(), e.sueldo - e.sueldo * 0.30)
        self.assertEqual(j.sueldo_tras_impuestos(), j.sueldo - (j.sueldo+j.bonus) * 0.30)

        # def comprueba_NIF(self):
   #     e = Persona("Pepe", 20000)
   #     j = Persona("Ana", 30000, "jefe", 2000)
   #     if self.assertIsNone(e.NIF) or self.assertIsNone(j.NIF):
   #         nif=input("No ha escrito su NIF por favor páselo a la terminal para evitar irregularidades")
   #         print(nif)
   #     else:
   #         pass

   # def comprueba_impuestos(self):
   #     e = Persona("Pepe", 20000)
   #     j = Persona("Ana", 30000, "jefe", 2000)
   #     self.assertEqual(e.calculo_impuestos[0],e.saldo*0.30)
   #     self.assertEqual(j.calculo_impuestos[0],(j.saldo+j.bonus)*0.30)

   # def comprueba_sueldo(self):
   #     e = Persona("Pepe", 20000)
   #     j = Persona("Ana", 30000, "jefe", 2000)
   #     self.assertEqual(e.calculo_impuestos[1], e.saldo-e.saldo * 0.30)
   #     self.assertEqual(j.calculo_impuestos[1],j.saldo-(j.saldo+j.bonus)*0.30)

