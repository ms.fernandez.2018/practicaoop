class Persona:

    def __init__(self, nombre, sueldo, puesto="empleado", bonus=0, NIF=None):
        self.nombre = nombre
        self.NIF = NIF
        self.sueldo=sueldo
        self.puesto=puesto
        self.bonus=bonus

    def calculo_impuestos(self):
        if self.puesto=="jefe":
            impuestos = (self.sueldo+self.bonus)*0.30
        else:
            impuestos = self.sueldo * 0.30

        return impuestos

    def sueldo_tras_impuestos(self):
        sueldo_tras_impuestos = self.sueldo - self.calculo_impuestos()
        return sueldo_tras_impuestos
    #def __str__(self):
    #    cadena="("+self.nombre+","+str(self.calculo_impuestos())+")"
    #    return cadena

    def __str__(self):
        cadena="El {puesto} {name} con NIF {NIF} debe pagar {tax}, con lo que queda una nómina de {sueldo" \
               "_tras_impuestos}".format(puesto=self.puesto,name=self.nombre, NIF=self.NIF,
tax=self.calculo_impuestos(), sueldo_tras_impuestos=self.sueldo_tras_impuestos())
        return cadena